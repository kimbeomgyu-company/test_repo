import * as functions from "firebase-functions";
import Axios from "axios";

/**
 * a-shop, b-shop에 scheduled push를 보낸다는 가정하에 작업
 * a-shop에 firebase error로 whereTo~ 에서 오류발생
 * b-shop에 firebase error로 whereTo~ 에서 오류발생
 */
const projectId = process.env.GCLOUD_PROJECT;

const endpoint =
  process.env.NODE_ENV === "production"
    ? `https://us-central1-${projectId}.cloudfunctions.net`
    : `http://localhost:5001/${projectId}/us-central1`;

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export const test__scheduled = functions.https.onRequest(async (request, response) => {
  console.log("=========================================================================");
  console.time("time:scheduled");
  const shopList = ["a-shop", "b-shop"];
  await sleep(10000); // db call 가정 10초

  const batchList = [];
  for (const shop of shopList) {
    try {
      const { status, data } = await Axios.get(`${endpoint}/test__whereTo?shop=${shop}`);
      if (status === 200) {
        batchList.push(shop);
        console.log({ data, message: "scheduled success response" });
      }
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        console.warn("scheduled response catch error", { status, data });
      } else {
        console.warn("scheduled 동작되지 않는 부분", { error });
      }
    }
  }

  console.timeEnd("time:scheduled");
  await sleep(10000); // batch commit call 가정 10초
  console.log("batch.commit", batchList);
  console.log("=============================end============================================");
  response.send("end !");
});

export const test__whereTo = functions.https.onRequest(async (request, response) => {
  const { shop } = request.query;
  console.time("time:whereTo " + shop);

  await sleep(10000); // db call 가정 10초

  await Promise.reject(new Error(`Firebase Read Error`));
  response.status(200).json({ status: 400, message: "Firebase Read Error" });
  return;

  const list = [];
  for (const pushEndpoint of ["sendPushSuccess"]) {
    try {
      console.log("whereTo request: " + shop);
      const { status, data } = await Axios.get(`${endpoint}/test__${pushEndpoint}`);
      if (status === 200) {
        console.log({ message: "whereTo", data });
      }
      list.push(1);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        console.warn("whereTo response error", { status, data });
      } else {
        console.warn("whereTo 동작이 되면 안됩니다", { error });
      }
      list.push(0);
    }
  }

  console.timeEnd("time:whereTo " + shop);

  if (list.includes(0)) {
    response.sendStatus(401);
    return;
  }

  response.sendStatus(200);
});

export const test__sendPushSuccess = functions.https.onRequest(async (request, response) => {
  console.time("time:send Success");
  await sleep(5000); // mobile messaging call 가정 5초

  console.timeEnd("time:send Success");
  response.sendStatus(200);
});
export const test__sendPushTimeout = functions.https.onRequest(async (request, response) => {
  console.time("timeout:send");
  await sleep(40000); // mobile messaging call 가정 40초

  console.timeEnd("timeout:send");
  response.send("error");
});

export const test__sendPushFailure = functions.https.onRequest(async (request, response) => {
  console.time("time:send Failure");
  await sleep(10); // mobile messaging call 가정 10초

  console.timeEnd("time:send Failure");
  response.sendStatus(500);
});

export const test__statusTest = functions.https.onRequest(async (request, response) => {
  for (const status of [
    /*
     * Status Code 문서
     * # Note
     * 1xx: Informational - Request received, continuing process
     * 2xx: Success - The action was successfully received, understood, and accepted
     * 3xx: Redirection - Further action must be taken in order to complete the request
     * 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
     * 5xx: Server Error - The server failed to fulfill an apparently valid request
     * https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     *
     * Shopify status code 문서
     * https://shopify.dev/concepts/about-apis/response-codes
     * 200, 201, 202, 303, 400, 401, 402, 403, 404, 406, 422, 423, 429, 500, 501, 503, 504
     */

    // ...[100, 101, 102, 103], // 104 ~ 199 Unassigned
    ...[200, 201, 202, 203, 204, 205, 206, 207, 208, 226], // 209-225, 227-299 Unassigned
    ...[300, 301, 302, 303, 304, 305, 306, 307, 308], // 309-399	Unassigned
    ...[400, 401, 402, 403, 404, 405, 406, 407, 408, 409],
    ...[410, 411, 412, 413, 414, 415, 416, 417], // 418-420	Unassigned
    ...[421, 422, 423, 424, 425, 426, 428, 429, 431, 451], // 427, 430, 432 ~ 450, 452 ~ 499 Unassigned
    ...[500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511], // 509, 512 ~ 599 Unassigned
  ]) {
    try {
      console.log("request: " + status);
      const { status: responseStatus, data, statusText } = await Axios.get(
        `${endpoint}/test__statusResponse?status=${status}`,
      );

      if (responseStatus !== 200) {
        console.log(JSON.stringify({ message: "status가 200이 아닌 상태", responseStatus, data, statusText }));
        continue;
      }

      console.log(responseStatus + " ok!!!!!!!!!!!!!!!!!");
    } catch (error) {
      const { status: errorStatus, statusText, data } = error.response;
      console.warn(
        JSON.stringify({
          message: "status가 catch에 잡힌 상태",
          errorMessage: error.message,
          errorStatus,
          statusText,
          data,
        }),
      );
    }
  }
  response.sendStatus(200);
});

export const test__statusResponse = functions.https.onRequest(async (request, response) => {
  const status = request.query.status;

  if (typeof status !== "string") {
    console.log("statusResponse type error..: string");
    response.sendStatus(200);
    return;
  }

  if (parseInt(status) === NaN) {
    console.log("statusResponse error..: number");
    response.sendStatus(200);
    return;
  }

  response.sendStatus(parseInt(status));
});
